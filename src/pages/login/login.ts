import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 //firebase conection
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { HomePage } from '../home/home';

//plujin
import { Platform } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private afAuth: AngularFireAuth, public userProvider:UsuarioProvider,
    private fb: Facebook, private platform: Platform,
    private googlePlus: GooglePlus) {
  }


//******************************************* facebook ******************************
//******************************************* facebook ******************************

  signInWithFacebook() {

    if (this.platform.is('cordova')) {
        this.fb.login(['email', 'public_profile']).then(res => {
        const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
         firebase.auth().signInWithCredential(facebookCredential).then(user=>{

          console.log(user);          
          this.userProvider.cargarUsuario(user.displayName,user.email,user.photoURL,user.uid,'Facebook');
          this.navCtrl.setRoot(HomePage);

         }).catch(e=>{console.log('Error con el login')});
      })
    }
    else {
      return this.afAuth.auth
        .signInWithPopup(new firebase.auth.FacebookAuthProvider())
        .then(res =>{ 
          console.log(res);
          let user=res.user;
          
          this.userProvider.cargarUsuario(user.displayName,user.email,user.photoURL,user.uid,'Facebook');
          this.navCtrl.setRoot(HomePage);

        });
    }

  }

  signOut() {
    this.afAuth.auth.signOut();
  }

//************************************* google plus ****************************
//************************************* google plus ****************************

loginGoogle(): void {
  this.googlePlus.login({
    'webClientId': 'yourwen client id',
    'offline': true
  }).then( res => {

    firebase.auth().signInWithCredential(firebase.auth.GoogleAuthProvider.credential(res.idToken))
      .then( user => {

        console.log("Firebase success: " + JSON.stringify(user));
        this.userProvider.cargarUsuario(user.displayName,user.email,user.photoURL,user.uid,'Facebook');
        this.navCtrl.setRoot(HomePage);
      })
      .catch( error => console.log("Firebase failure: " + JSON.stringify(error)));
  })
    .catch(err => console.error(err));
}

}
