import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UsuarioProvider, Credenciales } from '../../providers/usuario/usuario';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  user:any;
  constructor(public navCtrl: NavController,public usuairoProvider:UsuarioProvider) {

    console.log(this.usuairoProvider.usuario);
    this.user=this.usuairoProvider.usuario;
  }

}
